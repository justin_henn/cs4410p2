// Justin Henn
// cs4410 hw2
// 10/26/2017

#include <gl/glut.h>  

//display function 

void display(void) {

	//material lighting

	GLfloat mat_ambient[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	GLfloat mat_diffuse[] = { 1.0f, 1.0f, 0.3f, 1.0f };
	GLfloat mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat mat_shininess[] = { 50.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);


	//lighting

	GLfloat lightIntensity[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	GLfloat light_position[] = { -1.5f, 4.0f, 2.0f, 1.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightIntensity);

	//projection

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//glFrustum(-.5, .5, -winHt*factor, winHt*factor, 1, 100.0);

	gluPerspective(45, 1.0, 0.5, 100.0);

	//modelview

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
	gluLookAt(0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	mat_diffuse[0] = 1.0f, mat_diffuse[1] = 1.0f, mat_diffuse[2] = 1.0f, mat_diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glColorMaterial(GL_FRONT, GL_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);

	//variables for rotation and translation

	static float rotateBy = 0;
	rotateBy += .03;
	static float translate_by[] = { -0.4, -0.2, 0.0, 0.2, 0.4 };
	static float translate_by_middle[] = { -0.4, -0.2, 0.0, 0.2, 0.4 };

	for (int y = 0; y < 5; y++) {

		translate_by[y] += .0001;
		if (translate_by[y] > 0.6)
			translate_by[y] = -0.5;
	}
	for (int y = 0; y < 5; y++) {

		translate_by_middle[y] -= .0001;
		if (translate_by_middle[y] < -0.6)
			translate_by_middle[y] = 0.5;
	}

	//set shading and brown color

	glShadeModel(GL_SMOOTH);
	glColor3f(0.5f, 0.35f, 0.05f);


	/*glShadeModel(GL_FLAT);
	glBegin(GL_QUADS);
	glVertex3f(-.5, -.5, -.5);
	glVertex3f(.5, -.5, -.5);
	glVertex3f(.5, -.5, .5);
	glVertex3f(-.5, -.5, .5);

	/*glVertex3f(-.5, .5, -.5);
	glVertex3f(.5, .5, -.5);
	glVertex3f(.5, .5, .5);
	glVertex3f(-.5, .5, .5);

	glVertex3f(-.5, -.5, -.5);
	glVertex3f(.5, -.5, -.5);
	glVertex3f(.5, .5, -.5);
	glVertex3f(-.5, .5, -.5);

	glVertex3f(-.5, -.5, -.5);
	glVertex3f(.5, -.5, -.5);
	glVertex3f(.5, .5, -.5);
	glVertex3f(-.5, .5, -.5);

	glVertex3f(.5, .5, .5);
	glVertex3f(.5, -.5, .5);
	glVertex3f(.5, -.5, -.5);
	glVertex3f(.5, .5, -.5);

	glVertex3f(-.5, .5, .5);
	glVertex3f(-.5, -.5, .5);
	glVertex3f(-.5, -.5, -.5);
	glVertex3f(-.5, .5, -.5);
	glEnd();*/
	/*glPushMatrix();
	glScaled(-.5, -.5, -.5);
	glutSolidCube(1.0);
	glPopMatrix();*/


	//create cubes for wall

	glPushMatrix();
	glTranslated(0.0, -.5, -.5);
	glScaled(.5, 0.0, .5);
	glutSolidCube(2.2);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.0, .5, -.5);
	glScaled(.5, 0.0, .5);
	glutSolidCube(2.2);
	glPopMatrix();

	glPushMatrix();
	glTranslated(-0.5, 0.0, -.5);
	glScaled(0.0001, 0.5, 0.5);
	glRotated(90, 0, 0, 1);
	glutSolidCube(2.2);
	glPopMatrix();

	glPushMatrix();
	//glTranslated(0.5, 0.0, -.5);
	glScaled(0.0001, 0.5, 0.5);
	//glRotated(90, 0, 0, 1);
	glutSolidCube(2.2);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.5, 0.0, -0.5);
	glScaled(0.0001, 0.5, 0.5);
	//glRotated(90, 1, 0, 0);
	glutSolidCube(2.2);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.0, 0.0, -.5);
	glScaled(0.5, 0.5, 0.0);
	glutSolidCube(2.2);
	glPopMatrix();


	//create teapots


	glColor3f(0.0f, 1.0f, 1.0f);
	glPushMatrix();
	glTranslated((translate_by[0]), 0.3, 0.0);
	glRotated((2 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.2f, 0.05f, 1.0f);
	glPushMatrix();
	glTranslated((translate_by[1]), 0.3, 0.0);
	glRotated((50 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.25f, 0.02f, 0.44f);
	glPushMatrix();
	glTranslated(translate_by[2], 0.3, 0.0);
	glRotated((75 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.35f, 0.12f, 0.74f);
	glPushMatrix();
	glTranslated(translate_by[3], 0.3, 0.0);
	glRotated((90 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.75f, 0.22f, 0.11f);
	glPushMatrix();
	glTranslated(translate_by[4], 0.3, 0.0);
	glRotated((120 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.93f, 0.46f, 0.31f);
	glPushMatrix();
	glTranslated(translate_by_middle[0], 0.0, 0.0);
	glRotated((120 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.34f, 0.72f, 0.66f);
	glPushMatrix();
	glTranslated(translate_by_middle[1], 0.0, 0.0);
	glRotated((30 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.13f, 0.37f, 0.55f);
	glPushMatrix();
	glTranslated(translate_by_middle[2], 0.0, 0.0);
	glRotated((75 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.77f, 0.86f, 0.19f);
	glPushMatrix();
	glTranslated(translate_by_middle[3], 0.0, 0.0);
	glRotated((180 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.23f, 0.35f, 0.68f);
	glPushMatrix();
	glTranslated(translate_by_middle[4], 0.0, 0.0);
	glRotated((45 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.11f, 0.88f, 0.33f);
	glPushMatrix();
	glTranslated((translate_by[0]), -0.3, 0.0);
	glRotated((30 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.44f, 0.66f, 0.22f);
	glPushMatrix();
	glTranslated((translate_by[1]), -0.3, 0.0);
	glRotated((160 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.34f, 0.97f, 0.87f);
	glPushMatrix();
	glTranslated((translate_by[2]), -0.3, 0.0);
	glRotated((45 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.57f, 0.14f, 0.0f);
	glPushMatrix();
	glTranslated((translate_by[3]), -0.3, 0.0);
	glRotated((90 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();

	glColor3f(0.76f, 0.25f, 0.54f);
	glPushMatrix();
	glTranslated((translate_by[4]), -0.3, 0.0);
	glRotated((145 + rotateBy), 0, 1, 0);
	glutSolidTeapot(0.05);
	glPopMatrix();


	//draw and redisplay

	glFlush();
	glutSwapBuffers();
	glutPostRedisplay();
}

//main function

void main(int argc, char ** argv) {

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("teapots");
	glutDisplayFunc(display);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glViewport(0, 0, 640, 480);
	glutMainLoop();
}